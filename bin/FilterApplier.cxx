#include <iostream>

// dear imgui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <imfilebrowser.h>

#include <stdio.h>
#include <filesystem>

namespace fs = std::filesystem;

#include <Image.h>

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h> // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h> // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h> // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

static void glfw_error_callback(int error, const char *description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

// Simple helper function to load an image into a OpenGL texture with common settings
bool LoadTextureFromFile(const char *filename, GLuint *out_texture, int *out_width, int *out_height)
{
    // Load from file
    int image_width = 0;
    int image_height = 0;
    unsigned char *image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width = image_width;
    *out_height = image_height;

    return true;
}

// template <typename T, template <typename> class Pixel>
// bool LoadTextureFromFile(Image<T, Pixel> image, GLuint *out_texture, int *out_width, int *out_height)
// {
//     // Load from file
//     int image_width = 0;
//     int image_height = 0;
//     unsigned char *image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
//     if (image_data == NULL)
//         return false;

//     // Create a OpenGL texture identifier
//     GLuint image_texture;
//     glGenTextures(1, &image_texture);
//     glBindTexture(GL_TEXTURE_2D, image_texture);

//     // Setup filtering parameters for display
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

//     // Upload pixels into texture
//     glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
//     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
//     stbi_image_free(image_data);

//     *out_texture = image_texture;
//     *out_width = image_width;
//     *out_height = image_height;

//     return true;
// }

int main(int, char **)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // GL 3.0 + GLSL 130
    const char *glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

    // Create window with graphics context
    GLFWwindow *window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Our state
    bool show_demo_window = true;
    bool show_another_window = true;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // individual global variables
    // init of the file browser dialogs for opening and saving
    ImGuiFileBrowserFlags openFileDialogFlags = 0;
    openFileDialogFlags |= ImGuiFileBrowserFlags_CloseOnEsc;
    openFileDialogFlags |= ImGuiFileBrowserFlags_CreateNewDir;
    openFileDialogFlags |= ImGuiFileBrowserFlags_NoModal;

    ImGui::FileBrowser openFileDialog(openFileDialogFlags);

    openFileDialog.SetTitle("Open ...");
    openFileDialog.SetTypeFilters({".png", ".jpg", ".jpeg", ".bmp"});

    ImGuiFileBrowserFlags saveFileDialogFlags = 0;
    saveFileDialogFlags |= ImGuiFileBrowserFlags_CloseOnEsc;
    saveFileDialogFlags |= ImGuiFileBrowserFlags_EnterNewFilename;
    saveFileDialogFlags |= ImGuiFileBrowserFlags_CreateNewDir;
    saveFileDialogFlags |= ImGuiFileBrowserFlags_NoModal;

    ImGui::FileBrowser saveFileDialog(saveFileDialogFlags);

    saveFileDialog.SetTitle("Save ...");
    saveFileDialog.SetTypeFilters({".png", ".jpg", ".jpeg", ".bmp"});

    // path to image loaded
    fs::path image_path;

    // loaded image
    Image<unsigned char, RgbPixel> rgb_image;
    Image<unsigned char, HsvPixel> hsv_image;

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // create a window for an iamge
        {

            bool show_bar_file_open = false;
            bool show_bar_file_save = false;
            bool show_bar_file_saveas = false;
            bool show_bar_file_close = false;

            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_MenuBar;

            bool p_open = true;

            ImGui::Begin("FilterApplier", &p_open, window_flags);

            ImGui::PushItemWidth(ImGui::GetFontSize() * -12);

            /**
             * Menu Bar
             */

            // Menu Bar look
            if (ImGui::BeginMenuBar())
            {
                if (ImGui::BeginMenu("File"))
                {
                    ImGui::MenuItem("Open", NULL, &show_bar_file_open);
                    ImGui::MenuItem("Save", NULL, &show_bar_file_save);
                    ImGui::MenuItem("Save as..", NULL, &show_bar_file_saveas);
                    ImGui::MenuItem("Close", NULL, &show_bar_file_close);
                    ImGui::EndMenu();
                }
                if (ImGui::BeginMenu("Tools"))
                {
                    ImGui::EndMenu();
                }

                ImGui::EndMenuBar();
            }

            // Menu Bar logic
            if (show_bar_file_saveas)
            {
                saveFileDialog.Open();
            }

            if (show_bar_file_open)
            {
                openFileDialog.Open();
            }

            if (show_bar_file_close)
            {
                return 0;
            }

            saveFileDialog.Display();
            openFileDialog.Display();

            if (saveFileDialog.HasSelected())
            {
                std::cerr << "Selected filename to save " << saveFileDialog.GetSelected().string() << std::endl;

                if (!rgb_image.empty())
                {
                    rgb_image.store(saveFileDialog.GetSelected().string());
                }
                else if (!hsv_image.empty())
                {
                    hsv_image.store(saveFileDialog.GetSelected().string());
                }

                saveFileDialog.ClearSelected();
            }

            if (openFileDialog.HasSelected())
            {
                std::cerr << "Selected filename to open " << openFileDialog.GetSelected().string() << std::endl;
                image_path = saveFileDialog.GetSelected().string();
                rgb_image = Image<unsigned char, RgbPixel>(saveFileDialog.GetSelected().string());
                openFileDialog.ClearSelected();
            }

            // if theres an image -> load it
            if (!rgb_image.empty() || !hsv_image.empty())
            {
            }

            ImGui::End();
        }

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}