option(BUILD_GLFW_BACKEND "Build the library with the GLFW backend" ON)
option(BUILD_SDL_BACKEND "Build the library with the SDL backend" ON)

if(BUILD_SDL_BACKEND)
    find_package(SDL2 REQUIRED)
    include_directories(${SDL2_INCLUDE_DIR})
endif(BUILD_SDL_BACKEND)

if(BUILD_GLFW_BACKEND)
    find_package(GLFW REQUIRED)
    include_directories(${GLFW_INCLUDE_DIR})
endif(BUILD_GLFW_BACKEND)

find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)

file(GLOB_RECURSE SOURCE_CPP RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ext/imgui/*.cpp  )
file(GLOB_RECURSE SOURCE_HEADER RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ext/imgui/*.h  )


include_directories(${GLEW_INCLUDE_DIR})
include_directories( ext/imgui/ )

set( IMGUI_NAME "imgui")

add_library(${IMGUI_NAME} SHARED ${SOURCE_CPP} ${SOURCE_HEADER})

if(BUILD_SDL_BACKEND)
    target_link_libraries(${IMGUI_NAME} ${SDL2_LIBRARY})
endif(BUILD_SDL_BACKEND)

if(BUILD_GLFW_BACKEND)
    target_link_libraries(${IMGUI_NAME} ${GLFW_LIBRARY})
endif(BUILD_GLFW_BACKEND)

target_link_libraries(${IMGUI_NAME} ${GLEW_LIBRARY})
target_link_libraries(${IMGUI_NAME} ${OPENGL_LIBRARIES})
