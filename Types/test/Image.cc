#include <gtest/gtest.h>

#include <cstdlib>

#include <Image.h>

template <typename T, template <typename> class Pixel>
struct ImageTest : public ::testing::Test
{
protected:
    png::image<png::basic_rgb_pixel<unsigned char>> im_ref = png::image<png::basic_rgb_pixel<unsigned char>>("test_files/test.png");
    Image<T, Pixel> im = Image<T, Pixel>(std::string("test_files/test.png"));

    void SetUp() override {}
};

using ImageTestF = ImageTest<unsigned char, RgbPixel>;

TEST(Image, Basic)
{
    // create new png
    Image<unsigned char, RgbPixel> image(2, 3);

    image.getPixel(0, 0) = RgbPixel<unsigned int>(255, 0, 0);
    image.getPixel(0, 1) = RgbPixel<unsigned int>(0, 255, 0);
    image.getPixel(0, 2) = RgbPixel<unsigned int>(0, 0, 255);

    image.getPixel(1, 0) = RgbPixel<unsigned int>(0, 0, 255);
    image.getPixel(1, 1) = RgbPixel<unsigned int>(0, 255, 0);
    image.getPixel(1, 2) = RgbPixel<unsigned int>(255, 0, 0);

    const std::string file_name("test_files/2x3.png");
    image.store(file_name);

    Image<unsigned char, RgbPixel> im1(file_name);
    png::image<png::basic_rgb_pixel<unsigned char>> im2 = png::image<png::basic_rgb_pixel<unsigned char>>(file_name);

    EXPECT_EQ(image, im1);
    EXPECT_EQ(image, im2);
    EXPECT_EQ(im1, im2);
}

TEST_F(ImageTestF, GetSize)
{
    EXPECT_EQ(im.getWidth(), 1100);
    EXPECT_EQ(im.getHeight(), 619);
    EXPECT_EQ(im.getWidth(), im_ref.get_width());
    EXPECT_EQ(im.getHeight(), im_ref.get_height());
}

TEST_F(ImageTestF, GetPixel)
{
    for (auto i = 0; i < im.getHeight(); ++i)
    {
        for (auto j = 0; j < im.getWidth(); ++j)
        {
            EXPECT_EQ(im_ref[i][j].red, im.getPixel(i, j).getR());
            EXPECT_EQ(im_ref[i][j].green, im.getPixel(i, j).getG());
            EXPECT_EQ(im_ref[i][j].blue, im.getPixel(i, j).getB());
        }
    }
}

TEST_F(ImageTestF, Equal)
{

    Image<unsigned int, RgbPixel> im2(std::string("test_files/test.png"));
    png::image<png::basic_rgb_pixel<unsigned char>> im_ref2 = png::image<png::basic_rgb_pixel<unsigned char>>("test_files/test.png");

    EXPECT_EQ(im, im2);
    EXPECT_EQ(im, im_ref);
    EXPECT_EQ(im, im_ref2);
    EXPECT_EQ(im2, im_ref2);
}

TEST_F(ImageTestF, Store)
{
    im.store("test_files/tmp.png");

    Image<unsigned char, RgbPixel> t(std::string("test_files/tmp.png"));

    EXPECT_EQ(im, t);
    EXPECT_EQ(t, im_ref);
}
