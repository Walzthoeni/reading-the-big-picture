#pragma once

#include <png++/png.hpp>

template <typename T>
struct HsvPixel;

template <typename T>
struct RgbPixel;

namespace Detail
{

template <typename T>
HsvPixel<T>
RGBToHSV(const RgbPixel<T> &rgb)
{

    const auto &r = rgb.getR();
    const auto &g = rgb.getG();
    const auto &b = rgb.getB();

    HsvPixel<T> hsv;

    auto &h = hsv.getH();
    auto &s = hsv.getS();
    auto &v = hsv.getV();

    unsigned char rgbMin, rgbMax;

    rgbMin = r < g ? (r < b ? r : b) : (g < b ? g : b);
    rgbMax = r > g ? (r > b ? r : b) : (g > b ? g : b);

    v = rgbMax;
    if (v == 0)
    {
        h = 0;
        s = 0;
        return hsv;
    }

    s = 255 * long(rgbMax - rgbMin) / v;
    if (s == 0)
    {
        h = 0;
        return hsv;
    }

    if (rgbMax == r)
        h = 0 + 43 * (g - b) / (rgbMax - rgbMin);
    else if (rgbMax == g)
        h = 85 + 43 * (b - r) / (rgbMax - rgbMin);
    else
        h = 171 + 43 * (r - g) / (rgbMax - rgbMin);

    return hsv;
}

template <typename T>
HsvPixel<T>
RGBToHSV(const png::basic_rgb_pixel<T> &rgb)
{
    RgbPixel<T> tmp = rgb;
    return RGBToHSV(tmp);
}

template <typename T>
RgbPixel<T>
HSVtoRGB(const HsvPixel<T> &hsv)
{
    const auto &h = hsv.getH();
    const auto &s = hsv.getS();
    const auto &v = hsv.getV();

    RgbPixel<T> rgb;

    auto &r = rgb.getR();
    auto &g = rgb.getG();
    auto &b = rgb.getB();

    unsigned char region, remainder, p, q, t;

    if (s == 0)
    {
        r = v;
        g = v;
        b = v;
        return rgb;
    }

    region = h / 43;
    remainder = (h - (region * 43)) * 6;

    p = (v * (255 - s)) >> 8;
    q = (v * (255 - ((s * remainder) >> 8))) >> 8;
    t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
    case 0:
        r = v;
        g = t;
        b = p;
        break;
    case 1:
        r = q;
        g = v;
        b = p;
        break;
    case 2:
        r = p;
        g = v;
        b = t;
        break;
    case 3:
        r = p;
        g = q;
        b = v;
        break;
    case 4:
        r = t;
        g = p;
        b = v;
        break;
    default:
        r = v;
        g = p;
        b = q;
        break;
    }

    return rgb;
}

} // end namespace Detail

template <typename T>
struct Pixel
{
    using Type = T;
};

template <typename T>
struct RgbPixel : public Pixel<T>
{
private:
    T red;
    T green;
    T blue;

public:
    RgbPixel() = default;
    RgbPixel(const RgbPixel &) = default;
    RgbPixel(RgbPixel &&) = default;
    template <typename U>
    RgbPixel(const RgbPixel<U> &o)
    {
        red = o.getR();
        green = o.getG();
        blue = o.getB();
    }
    template <typename U>
    RgbPixel(const HsvPixel<U> &hsv)
    {
        auto tmp = Detail::HSVtoRGB(hsv);

        red = tmp.getR();
        green = tmp.getG();
        blue = tmp.getB();
    }
    template <typename U>
    RgbPixel(const png::basic_rgb_pixel<U> &in) : red(in.red), green(in.green), blue(in.blue) {}
    template <typename U>
    RgbPixel(png::basic_rgb_pixel<U> &in) : red(in.red), green(in.green), blue(in.blue) {}
    RgbPixel(const T &r, const T &g, const T &b) : red(r), green(g), blue(b) {}

    RgbPixel &operator=(const RgbPixel &) = default;
    template <typename U>
    RgbPixel &operator=(const HsvPixel<U> &hsv)
    {
        auto tmp = Detail::HSVtoRGB(hsv);

        red = tmp.getR();
        green = tmp.getG();
        blue = tmp.getB();

        return *this;
    }
    RgbPixel &operator=(png::basic_rgb_pixel<T> &in)
    {
        red = in.red;
        green = in.green;
        blue = in.blue;

        return *this;
    }

    T operator[](const std::size_t &p) const
    {
        assert(p >= 0 && p < 3 && "index out of bounds");
        if (p == 0)
        {
            return red;
        }
        else if (p == 1)
        {
            return green;
        }
        else if (p == 2)
        {
            return blue;
        }

        return 0;
    }

    auto &getR() { return red; }
    auto &getG() { return green; }
    auto &getB() { return blue; }
    const auto &getR() const { return red; }
    const auto &getG() const { return green; }
    const auto &getB() const { return blue; }

    HsvPixel<T> convertToHsv() const
    {
        return Detail::RGBToHSV(*this);
    }
};

template <typename T>
struct HsvPixel : public Pixel<T>
{
private:
    T hue;
    T saturation;
    T value;

public:
    HsvPixel() = default;
    HsvPixel(const HsvPixel &) = default;
    HsvPixel(HsvPixel &&) = default;
    template <typename U>
    HsvPixel(const png::basic_rgb_pixel<U> &in)
    {
        *this = in;
    }
    template <typename U>
    HsvPixel(const RgbPixel<U> &in)
    {
        *this = in;
    }
    HsvPixel &operator=(const HsvPixel &) = default;
    template <typename U>
    HsvPixel &operator=(const RgbPixel<U> &rgb)
    {
        auto tmp = Detail::RGBToHSV(rgb);

        hue = tmp.getH();
        saturation = tmp.getS();
        value = tmp.getV();

        return *this;
    }
    template <typename U>
    HsvPixel &operator=(const png::basic_rgb_pixel<U> &rgb)
    {
        auto tmp = Detail::RGBToHSV(rgb);

        hue = tmp.getH();
        saturation = tmp.getS();
        value = tmp.getV();

        return *this;
    }

    T operator[](const std::size_t &p) const
    {
        assert(p >= 0 && p < 3 && "index out of bounds");
        if (p == 0)
        {
            return hue;
        }
        else if (p == 1)
        {
            return saturation;
        }
        else if (p == 2)
        {
            return value;
        }

        return 0;
    }

    auto &getH() { return hue; }
    auto &getS() { return saturation; }
    auto &getV() { return value; }
    const auto &getH() const { return hue; }
    const auto &getS() const { return saturation; }
    const auto &getV() const { return value; }

    RgbPixel<T> convertToRgb() const
    {
        return Detail::HSVtoRGB(*this);
    }
};