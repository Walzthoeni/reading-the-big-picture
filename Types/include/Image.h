#pragma once

#include <png++/png.hpp>
#include <cstdlib>
#include <vector>
#include <filesystem>

#include <runtime_assert.h>
#include <Pixel.h>

template <typename T, template <typename> class Pixel>
struct Image;

template <typename T, typename U>
Image<T, HsvPixel> createHsvImage(const Image<U, RgbPixel> &p)
{
  Image<T, HsvPixel> res(p.getHeight(), p.getWidth());

  for (auto i = 0; i < p.getHeight(); ++i)
  {
    for (auto j = 0; j < p.getWidth(); ++j)
    {
      res.getPixel(i, j) = HsvPixel<T>(p.getPixel(i, j));
    }
  }

  return res;
}

template <typename T, typename U>
Image<T, RgbPixel> createRgbImage(const Image<U, HsvPixel> &p)
{
  Image<T, RgbPixel> res;

  for (auto i = 0; i < p.getHeight(); ++i)
  {
    for (auto j = 0; j < p.getWidth(); ++j)
    {
      res.getPixel(i, j) = RgbPixel<T>(p.getPixel(i, j));
    }
  }

  return res;
}

template <typename T, template <typename> class Pixel>
struct Image
{
private:
  std::size_t height;
  std::size_t width;

  std::filesystem::path input_path;
  std::vector<Pixel<T>> data;

  void resize()
  {
    data.clear();

    for (auto i = 0; i < height * width; ++i)
    {
      data.push_back(Pixel<T>());
    }
  }

  bool load()
  {
    // check if the file is valid

    if (!std::filesystem::exists(input_path))
    {
      //something went wrong
      return false;
    }

    // load the file with the libpng
    png::image<png::rgb_pixel> im(input_path);

    // set sizes
    height = im.get_height();
    width = im.get_width();

    data.clear();

    // convert the data
    if (typeid(Pixel<T>) == typeid(HsvPixel<T>))
    {
      for (auto i = 0; i < im.get_height(); ++i)
      {
        for (auto j = 0; j < im.get_width(); ++j)
        {
          data.push_back(Pixel<T>(Detail::RGBToHSV(im[i][j])));
        }
      }
    }
    else if (typeid(Pixel<T>) == typeid(RgbPixel<T>))
    {
      for (auto i = 0; i < im.get_height(); ++i)
      {
        for (auto j = 0; j < im.get_width(); ++j)
        {
          data.push_back(Pixel<T>(im[i][j]));
        }
      }
    }
    else
    {
      return false;
    }

    return true;
  }

public:
  Image() = default;
  Image(const Image &) = default;
  Image(Image &&) = default;

  Image &operator=(const Image &) = default;

  Image(const std::size_t &h, const std::size_t &w) : height(h), width(w)
  {
    resize();
  }

  Image(const std::string &in) : input_path(in)
  {
    load();
  }

  Image(const std::filesystem::path &in) : input_path(in)
  {
    load();
  }

  const bool empty() const
  {
    return height == 0 || width == 0 || data.size() == 0;
  }

  void store(const std::string &p) const
  {
    png::image<png::rgb_pixel> tmp(getWidth(), getHeight());

    // convert the data
    if (typeid(Pixel<T>) == typeid(HsvPixel<T>))
    {
      for (auto i = 0; i < getHeight(); ++i)
      {
        for (auto j = 0; j < getWidth(); ++j)
        {
          auto t = Detail::HSVtoRGB(HsvPixel<T>(getPixel(i, j)));
          tmp[i][j].red = t[0];
          tmp[i][j].green = t[1];
          tmp[i][j].blue = t[2];
        }
      }
    }
    else if (typeid(Pixel<T>) == typeid(RgbPixel<T>))
    {
      for (auto i = 0; i < getHeight(); ++i)
      {
        for (auto j = 0; j < getWidth(); ++j)
        {
          auto t = getPixel(i, j);
          tmp[i][j].red = t[0];
          tmp[i][j].green = t[1];
          tmp[i][j].blue = t[2];
        }
      }
    }

    tmp.write(p);
  }

  std::vector<Pixel<T>> &getData()
  {
    return data;
  }

  const std::vector<Pixel<T>> &getData() const
  {
    return data;
  }

  std::size_t size() const
  {
    return data.size();
  }

  Pixel<T> &getPixel(const std::size_t &row, const std::size_t &column)
  {
    assert(row * width + column < data.size() && "index too high");
    assert(row < width && column < height && "one of the indcies is out of bounds");
    return data[row * width + column];
  }

  const Pixel<T> &getPixel(const std::size_t &row, const std::size_t &column) const
  {
    assert(row * width + column < data.size() && "index too high");
    assert(row < width && column < height && "one of the indcies is out of bounds");
    return data[row * width + column];
  }

  Pixel<T> &getPixel(const std::size_t &pos)
  {
    assert(pos < data.size() && "index too high");
    return data[pos];
  }

  const Pixel<T> &getPixel(const std::size_t &pos) const
  {
    assert(pos < data.size() && "index too high");
    return data[pos];
  }

  std::size_t &getHeight() { return height; }
  std::size_t &getWidth() { return width; }
  const std::size_t &getHeight() const { return height; }
  const std::size_t &getWidth() const { return width; }
};

template <typename T, typename U>
bool operator==(const Image<T, RgbPixel> &a, const Image<U, RgbPixel> &b)
{
  if (b.getHeight() != a.getHeight() || b.getWidth() != a.getWidth())
  {
    return false;
  }

  for (auto i = 0; i < a.getHeight(); ++i)
  {
    for (auto j = 0; j < a.getWidth(); ++j)
    {
      if (b.getPixel(i, j).getR() != a.getPixel(i, j).getR() || b.getPixel(i, j).getG() != a.getPixel(i, j).getG() ||
          b.getPixel(i, j).getB() != a.getPixel(i, j).getB())
      {
        return false;
      }
    }
  }
  return true;
}

template <typename T, typename U>
bool operator!=(const Image<T, RgbPixel> &a, const Image<U, RgbPixel> &b)
{
  return !(a == b);
}

template <typename T, typename U>
bool operator==(const Image<T, HsvPixel> &a, const Image<U, HsvPixel> &b)
{
  if (b.getHeight() != a.getHeight() || b.getWidth() != a.getWidth())
  {
    return false;
  }

  for (auto i = 0; i < a.getHeight(); ++i)
  {
    for (auto j = 0; j < a.getWidth(); ++j)
    {
      if (b.getPixel(i, j).getH() != a.getPixel(i, j).getH() || b.getPixel(i, j).getS() != a.getPixel(i, j).getS() ||
          b.getPixel(i, j).getV() != a.getPixel(i, j).getV())
      {
        return false;
      }
    }
  }
  return true;
}

template <typename T, typename U>
bool operator!=(const Image<T, HsvPixel> &a, const Image<U, HsvPixel> &b)
{
  return !(a == b);
}

template <typename T, typename U>
bool operator==(const Image<T, RgbPixel> &a, const Image<U, HsvPixel> &b)
{
  Image<U, RgbPixel> tmp(b);
  return tmp == a;
}

template <typename T, typename U>
bool operator!=(const Image<T, RgbPixel> &a, const Image<U, HsvPixel> &b)
{
  return !(a == b);
}

template <typename T, typename U>
bool operator==(const Image<U, HsvPixel> &b, const Image<T, RgbPixel> &a)
{
  return a == b;
}

template <typename T, typename U>
bool operator!=(const Image<U, HsvPixel> &b, const Image<T, RgbPixel> &a)
{
  return !(a == b);
}

template <typename T, typename U>
bool operator==(const Image<T, RgbPixel> &a, const png::image<png::basic_rgb_pixel<U>> &b)
{
  if (b.get_height() != a.getHeight() || b.get_width() != a.getWidth())
  {
    return false;
  }

  std::size_t counter = 0;

  for (auto i = 0; i < a.getHeight(); ++i)
  {
    for (auto j = 0; j < a.getWidth(); ++j)
    {
      if (b[i][j].red != a.getPixel(i, j).getR() || b[i][j].green != a.getPixel(i, j).getG() ||
          b[i][j].blue != a.getPixel(i, j).getB())
      {
        return false;
      }
      counter++;
    }
  }
  return true;
}

template <typename T, typename U>
bool operator!=(const Image<T, RgbPixel> &a, const png::image<png::basic_rgb_pixel<U>> &b)
{
  return !(a == b);
}

template <typename T, typename U, template <typename> class Pixel>
bool operator==(const png::image<png::basic_rgb_pixel<U>> &o, const Image<T, Pixel> &i)
{
  return i == o;
}

template <typename T, typename U, template <typename> class Pixel>
bool operator!=(const png::image<png::basic_rgb_pixel<U>> &o, const Image<T, Pixel> &i)
{
  return !(i == o);
}
