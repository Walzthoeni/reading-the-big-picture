#pragma once

#include <png++/png.hpp>

#include <Image.h>

template <typename T, template <typename> class P>
struct Image;

struct RedChannel
{
  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &pos)
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].red;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getR();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].red;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getR();
  }

  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &h, const std::size_t &w)
  {
    return im[h][w].red;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getR();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im[h][w].red;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getR();
  }
};

struct BlueChannel
{
  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &pos)
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].blue;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getB();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].blue;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getB();
  }

  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &h, const std::size_t &w)
  {
    return im[h][w].blue;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getB();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im[h][w].blue;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getB();
  }
};

struct GreenChannel
{
  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &pos)
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].green;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getG();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.get_width();
    const auto h = im.get_height();

    return im[pos % h][pos / w].green;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getG();
  }

  template <typename Color>
  Color &operator()(png::image<png::basic_rgb_pixel<Color>> &im, const std::size_t &h, const std::size_t &w)
  {
    return im[h][w].green;
  }

  template <typename Color>
  Color &operator()(Image<Color, RgbPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getG();
  }

  template <typename Color>
  const Color &operator()(const png::image<png::basic_rgb_pixel<Color>> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im[h][w].green;
  }

  template <typename Color>
  const Color &operator()(const Image<Color, RgbPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getG();
  }
};

struct HueChannel
{
  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getH();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getH();
  }

  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getH();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getH();
  }
};

struct SatChannel
{
  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getS();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getS();
  }

  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getS();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getS();
  }
};

struct ValChannel
{
  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &pos)
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getV();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &pos) const
  {
    const auto w = im.getWidth();
    const auto h = im.getHeight();

    return im.getPixel(pos % h, pos / w).getV();
  }

  template <typename Color>
  Color &operator()(Image<Color, HsvPixel> &im, const std::size_t &h, const std::size_t &w)
  {
    return im.getPixel(h, w).getV();
  }

  template <typename Color>
  const Color &operator()(const Image<Color, HsvPixel> &im,
                          const std::size_t &h, const std::size_t &w) const
  {
    return im.getPixel(h, w).getV();
  }
};