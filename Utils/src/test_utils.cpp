#include <test_utils.h>

#include <fstream>
#include <iostream>

#include <stb_image.h>

bool compare_files(const std::string &filename_a, const std::string &filename_b)
{

    std::ifstream in1(filename_a, std::ios::binary);
    std::ifstream in2(filename_b, std::ios::binary);

    unsigned char i1, i2;

    if (!in1.good() || !in2.good())
    {
        std::cerr << "no good!" << std::endl;
        return false;
    }

    if (in1.tellg() != in2.tellg())
    {
        std::cerr << "no size!" << std::endl;
        return false;
    }

    std::size_t counter = 0;
    while (in1 >> i1 && in2 >> i2)
    {
        if (i1 != i2)
        {
            std::cerr << "no content! c : " << counter << std::endl;
            return false;
        }
        counter++;
    }

    return true;
}

bool compare_png_content(const std::string &filename_a, const std::string &filename_b)
{

    int width1, height1, channels1;
    int width2, height2, channels2;

    const std::string orig_filename = "test_files/landscape1.png";
    const std::string test_filename = "test_files/output.png";

    unsigned char *i1 = stbi_load(filename_a.c_str(),
                                  &width1,
                                  &height1,
                                  &channels1,
                                  0);

    unsigned char *i2 = stbi_load(filename_b.c_str(),
                                  &width2,
                                  &height2,
                                  &channels2,
                                  0);

    if (!i1 || !i2)
    {
        std::cerr << "file/s not existing or other loading related problem" << std::endl;
        return false;
    }

    if (width1 != width2 || height1 != height2 || channels1 != channels2)
    {
        std::cerr << "files have different sizes!" << std::endl;
        return false;
    }

    std::size_t counter = 0;
    for (auto i = 0; i < height1 * width1; ++i)
    {
        if (i1[i] != i2[i])
        {
            std::cerr << "pixel " << counter << " mismatched!" << std::endl;
            return false;
        }
    }

    return true;
}