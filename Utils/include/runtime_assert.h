#pragma once

#include <iostream>

inline void _assert(const char* expression, const char* file, int line)
{
	std::cerr << "Assertion " << expression << " failed, file " << file << " line " << line << ".";
	exit(1);
}
 
#ifndef NDEBUG
#define assert(EXPRESSION) ((void)0)
#else
#define assert(EXPRESSION) ((EXPRESSION) ? (void)0 : _assert(#EXPRESSION, __FILE__, __LINE__))
#endif