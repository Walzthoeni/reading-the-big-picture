#pragma once

#include <string>

bool compare_files(const std::string &filename_a, const std::string &filename_b);
bool compare_png_content(const std::string &filename_a, const std::string &filename_b);
