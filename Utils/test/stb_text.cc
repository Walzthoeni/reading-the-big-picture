#include <gtest/gtest.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <Image.h>
#include <Pixel.h>

#include <test_utils.h>

TEST(stb_image, Basic)
{
    int width, height, channels;

    const std::string orig_filename = "test_files/landscape1.png";
    const std::string test_filename = "test_files/output.png";

    unsigned char *image = stbi_load(orig_filename.c_str(),
                                     &width,
                                     &height,
                                     &channels,
                                     0);

    // check that thing
    EXPECT_EQ(1100, width);
    EXPECT_EQ(619, height);
    EXPECT_EQ(3, channels);

    stbi_write_png(test_filename.c_str(), width, height, channels, image, 0);
    stbi_image_free(image);

    EXPECT_TRUE(compare_png_content(orig_filename, test_filename));
}

TEST(stb_image, stbi_load)
{
    // Load from file
    const std::string filename = "test_files/landscape1.png";

    int image_width = 0;
    int image_height = 0;
    int image_channels = 0;
    unsigned char *image_data = stbi_load(filename.c_str(), &image_width, &image_height, &image_channels, 3);

    if (image_data == NULL)
    {
        ASSERT_TRUE(false) << "file not existing";
    }

    Image<unsigned char, RgbPixel> im(filename);

    EXPECT_EQ(image_width, im.getWidth());
    EXPECT_EQ(image_height, im.getHeight());
    EXPECT_EQ(image_channels, 3);

    const std::size_t pic_size = image_width * image_height;

    for (auto i = 0; i < image_height; ++i)
    {
        for (auto j = 0; j < image_width; ++j)
        {
            const auto pos = i * image_width + j;

            const auto pos_r = 0 + 3 * pos;
            const auto pos_g = 1 + 3 * pos;
            const auto pos_b = 2 + 3 * pos;

            const auto pi_r = image_data[pos_r];
            const auto pi_g = image_data[pos_g];
            const auto pi_b = image_data[pos_b];

            EXPECT_EQ(pi_r, im.getPixel(pos).getR()); // red
            EXPECT_EQ(pi_g, im.getPixel(pos).getG()); // green
            EXPECT_EQ(pi_b, im.getPixel(pos).getB()); // blue
        }
    }
}