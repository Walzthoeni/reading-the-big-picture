#include <gtest/gtest.h>

#include <CreateLsbPng.h>
#include <Channel.h>
#include <Image.h>
#include <LSB.h>

using namespace Validation;

TEST(CreateLsbPng, Basic)
{
    const std::string input_image{"test_files/lsb_input.png"};
    const std::string reference_image{"test_files/lsb_reference.png"};
    const std::string created_image{"test_files/lsb_created.png"};

    // load a picute and create a new b/w one accoring to the LSB
    CreateLsbPng<RedChannel> lsb_picture(input_image);

    // store the picutre
    lsb_picture.store(created_image);

    // compare the generated picture with a reference pic
    png::image<png::rgb_pixel> r_image = png::image<png::rgb_pixel>(reference_image);
    png::image<png::rgb_pixel> c_image = png::image<png::rgb_pixel>(created_image);

    ASSERT_EQ(r_image.get_width(), c_image.get_width());
    ASSERT_EQ(r_image.get_height(), c_image.get_height());

    for (auto row = 0; row < r_image.get_height(); ++row)
    {
        for (auto column = 0; column < r_image.get_width(); ++column)
        {
            EXPECT_EQ(r_image[row][column].red, c_image[row][column].red);
            EXPECT_EQ(r_image[row][column].green, c_image[row][column].green);
            EXPECT_EQ(r_image[row][column].blue, c_image[row][column].blue);
        }
    }
}