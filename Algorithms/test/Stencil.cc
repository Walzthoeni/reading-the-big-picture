#include <gtest/gtest.h>

#include <Image.h>
#include <Channel.h>
#include <Stencils/Stencil.h>
#include <Stencils/Blur.h>

using PixelSize = std::size_t;

template <typename T, typename U>
bool compare_epsilon(const Image<T, RgbPixel> &a, const Image<U, RgbPixel> &b, const double &epsilon)
{
    if (b.getHeight() != a.getHeight() || b.getWidth() != a.getWidth())
    {
        std::cout << "wrong size" << std::endl;
        return false;
    }

    bool res = true;

    for (auto i = 0; i < a.getHeight(); ++i)
    {
        for (auto j = 0; j < a.getWidth(); ++j)
        {
            double val = 0;

            const auto &ap = a.getPixel(i, j);
            const auto &bp = b.getPixel(i, j);

            val += std::abs((double)bp.getR() - (double)ap.getR());
            val += std::abs((double)bp.getG() - (double)ap.getG());
            val += std::abs((double)bp.getB() - (double)ap.getB());

            if (val > epsilon)
            {

                std::cout << "wrong pixels at [" << i << "," << j << "] w ";
                std::cout << "a [" << ap.getR() << "," << ap.getG() << "," << ap.getB() << "] != ";
                std::cout << "b [" << bp.getR() << "," << bp.getG() << "," << bp.getB() << "]";
                std::cout << " val : " << val << std::endl;
                res = false;
            }
        }
    }
    return res;
}

TEST(EvenBlurFilter, Basic)
{
    const Image<PixelSize, RgbPixel> im(std::string("test_files/landscape1.png"));

    // create filtered picture
    auto res = Stencil<RedChannel, GreenChannel, BlueChannel>()(im, EvenBlurFilter()(3));

    EXPECT_EQ(im.getHeight(), res.getHeight());
    EXPECT_EQ(im.getWidth(), res.getWidth());

    EXPECT_NE(im, res);

    const double f = 1 / 9.0;

    const std::array<std::array<double, 3>, 3> filter{
        std::array<double, 3>{f, f, f},
        std::array<double, 3>{f, f, f},
        std::array<double, 3>{f, f, f}};

    auto res_s1 = Stencil<RedChannel, GreenChannel, BlueChannel>()(im, filter);

    EXPECT_EQ(im.getHeight(), res_s1.getHeight());
    EXPECT_EQ(im.getWidth(), res_s1.getWidth());

    // needed cause of rounding errors while processing
    EXPECT_TRUE(compare_epsilon(res, res_s1, 3));

    // check outcome pixel
    const Image<PixelSize, RgbPixel> ref(std::string("test_files/EvenBlurFilter_reference.png"));

    // needed cause of rounding errors while processing
    EXPECT_TRUE(compare_epsilon(ref, res_s1, 3));
    EXPECT_TRUE(compare_epsilon(ref, res, 3));
}

TEST(GaussianBlurFilter, Basic)
{

    const Image<PixelSize, RgbPixel> im(std::string("test_files/landscape1.png"));

    // create filtered picture
    const auto gaussianBlurFilter = GaussianBlurFilter()(3);
    auto res = Stencil<RedChannel, GreenChannel, BlueChannel>()(im, gaussianBlurFilter);

    EXPECT_EQ(im.getHeight(), res.getHeight());
    EXPECT_EQ(im.getWidth(), res.getWidth());

    EXPECT_NE(im, res);

    const std::array<std::array<double, 3>, 3> filter{
        std::array<double, 3>{0.0751136, 0.123841, 0.0751136},
        std::array<double, 3>{0.123841, 0.20418, 0.123841},
        std::array<double, 3>{0.0751136, 0.123841, 0.0751136}};

    auto res_s1 = Stencil<RedChannel, GreenChannel, BlueChannel>()(im, filter);

    EXPECT_EQ(im.getHeight(), res_s1.getHeight());
    EXPECT_EQ(im.getWidth(), res_s1.getWidth());

    res_s1.store("test_files/GaussianBlurFilter_reference.png");
    // needed cause of rounding errors while processing
    EXPECT_TRUE(compare_epsilon(res, res_s1, 3));

    // check outcome pixel
    const Image<PixelSize, RgbPixel> ref(std::string("test_files/GaussianBlurFilter_reference.png"));

    // needed cause of rounding errors while processing
    EXPECT_TRUE(compare_epsilon(ref, res_s1, 3));
    EXPECT_TRUE(compare_epsilon(ref, res, 3));
}
