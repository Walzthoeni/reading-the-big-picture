#pragma once

#include <cmath>

#include <Image.h>
#include <Stencils/Filter.h>

struct EvenBlurFilter
{
    template <std::size_t s>
    Filter operator()()
    {
        return operator()(s);
    }

    Filter operator()(const std::size_t &s)
    {
        Filter res(s);

        for (auto i = 0; i < s; ++i)
        {
            for (auto j = 0; j < s; ++j)
            {
                res[i][j] = 1 / ((double)(s * s));
            }
        }

        return res;
    }
};

struct GaussianBlurFilter
{
    template <std::size_t s>
    const Filter operator()() const
    {
        return operator()(s);
    }

    const Filter operator()(const std::size_t &s) const
    {
        Filter res(s, s);

        double sigma = 1;

        double mean = s / 2;
        double sum = 0.0;

        for (int i = 0; i < s; ++i)
            for (int j = 0; j < s; ++j)
            {
                res[i][j] = std::exp(-0.5 * (std::pow((i - mean) / sigma, 2.0) + std::pow((j - mean) / sigma, 2.0))) / (2 * M_PI * sigma * sigma);
                sum += res[i][j];
            }

        // Normalize the kernel
        for (int i = 0; i < s; ++i)
            for (int j = 0; j < s; ++j)
                res[i][j] /= sum;

        return res;
    }
};
