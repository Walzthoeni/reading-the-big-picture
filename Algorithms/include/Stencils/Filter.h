#pragma once

#include <cstdlib>
#include <vector>

#include <runtime_assert.h>

struct Filter
{
    std::vector<std::vector<double>> f;

    const std::size_t height;
    const std::size_t width;

    Filter() = delete;

    Filter(const std::size_t &s) : height(s), width(s)
    {
        if (height % 2 == 0 || width % 2 == 0)
        {
            assert(true && "wrong filter size");
        }
        for (auto i = 0; i < height; ++i)
        {
            f.push_back(std::vector<double>());

            for (auto j = 0; j < width; ++j)
            {
                f[i].push_back(0.0);
            }
        }
    }

    Filter(const std::size_t &h, const std::size_t &w) : height(h), width(w)
    {
        if (height % 2 == 0 || width % 2 == 0)
        {
            assert(true && "wrong filter size");
        }
        for (auto i = 0; i < height; ++i)
        {
            f.push_back(std::vector<double>());

            for (auto j = 0; j < width; ++j)
            {
                f[i].push_back(0.0);
            }
        }
    }

    Filter(const std::vector<std::vector<double>> &o) : height(o.size()), width(o[0].size())
    {
        if (o.size() % 2 == 0 || o[0].size() % 2 == 0)
        {
            assert(true && "wrong filter size");
        }
        // check the format
        if (!o.size())
        {
            assert(true && "wrong filter format");
        }

        const std::size_t row_size = o[0].size();

        for (auto i = 1; i < o.size(); ++i)
        {
            if (row_size != o[i].size())
            {
                assert(true && "wrong filter format");
            }
        }

        f = o;
    }

    template <std::size_t h, std::size_t w>
    Filter(const std::array<std::array<double, w>, h> &a) : height(h), width(w)
    {
        if (h % 2 == 0 || w % 2 == 0)
        {
            assert(true && "wrong filter size");
        }

        for (auto i = 0; i < h; ++i)
        {
            std::vector<double> tmp;

            for (auto j = 0; j < w; ++j)
            {
                tmp.push_back(a[i][j]);
            }

            f.push_back(tmp);
        }
    }

    std::vector<double> &operator[](const std::size_t &p) { return f[p]; }
    const std::vector<double> &operator[](const std::size_t &p) const { return f[p]; }

    const std::size_t &getHeight() const { return height; }
    const std::size_t &getWidth() const { return width; }
};