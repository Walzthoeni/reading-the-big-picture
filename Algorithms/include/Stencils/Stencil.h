#pragma once

#include <cstdlib>
#include <vector>

#include <Image.h>
#include <Stencils/Filter.h>

struct Filter;

namespace Detail
{

template <typename... Channels>
struct ChannelApplier;

template <typename... Channels>
struct FilterApplier
{
    template <typename T>
    void operator()(const Filter &f, const T &input, T &output) const
    {
        for (auto row = 1; row < input.getHeight() - 1; ++row)
        {
            for (auto column = 1; column < input.getWidth() - 1; ++column)
            {
                Detail::ChannelApplier<Channels...>()(row, column, f, input, output);
            }
        }
    }
};

template <typename First, typename... Rest>
struct ChannelApplier<First, Rest...>
{
    template <typename T, template <typename> class Pixel>
    void operator()(const std::size_t &p_x, const std::size_t &p_y, const Filter &f, const Image<T, Pixel> &input, Image<T, Pixel> &output) const
    {
        const auto f_h = f.getHeight();
        const auto f_h_m = f_h / 2;
        const auto f_w = f.getWidth();
        const auto f_w_m = f_w / 2;

        // compute single value
        double val = 0;
        for (auto i = 0; i < f_h; ++i)
        {
            const auto pic_pos_h = p_x - i + f_h_m;
            const auto fil_pos_h = i;

            for (auto j = 0; j < f_w; ++j)
            {
                const auto pic_pos_w = p_y - j + f_w_m;
                const auto fil_pos_w = j;

                val += First()(input, pic_pos_h, pic_pos_w) * f[fil_pos_h][fil_pos_w];
            }
        }

        // store the value
        First()(output, p_x, p_y) = val;

        // apply the filter on the same position for a different filter
        ChannelApplier<Rest...>()(p_x, p_y, f, input, output);
    }
};

template <>
struct ChannelApplier<>
{
    template <typename T, template <typename> class Pixel>
    void operator()(const std::size_t &p_x, const std::size_t &p_y, const Filter &f, const Image<T, Pixel> &input, Image<T, Pixel> &output) const {}
};

} // end namespace Detail

template <typename... Channels>
struct Stencil
{
    template <typename T, template <typename> class Pixel>
    Image<T, Pixel> operator()(const Image<T, Pixel> &in, const Filter &f)
    {
        Image<T, Pixel> res(in);

        Detail::FilterApplier<Channels...>()(f, in, res);

        return res;
    }
};