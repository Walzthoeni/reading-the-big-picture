#pragma once

#include <png++/png.hpp>
#include <png.h>

template <typename Channel>
struct LSB
{

  /**
   * This function will embed a given message msg into a picture and return it
   */
  template <typename T>
  static png::image<T> encode(const png::image<T> &input,
                              const std::string &msg)
  {
    // check if message is shorter than the picture you want to hide it in
    if (msg.size() * 8 > input.get_height() * input.get_width())
    {
      std::cerr << "pic is too small for encoding" << std::endl;
      return png::image<T>();
    }

    // create result
    auto res = input;

    // hide stuff
    std::size_t counter = 0;
    for (const auto &c : msg)
    {
      for (auto i = 0; i < sizeof(c) * 8; ++i)
      {
        const auto op = ((c & (1 << (7 - i))) != 0);
        auto &elem = Channel()(res, counter);
        elem = (elem & ~1) | (op & 1);
        counter++;
      }
    }

    return res;
  }

  /**
   * This function provides will decode the whole picture and return the
   * resulting string
   */
  template <typename T>
  static std::string decode(const png::image<T> &input)
  {
    // create a return string
    std::string res;

    auto tmp_bit_counter = 0;
    char tmp = 0;

    for (auto i = 0; i < input.get_height() * input.get_width(); ++i)
    {
      // set bitwise the information into a char
      tmp |= ((Channel()(input, i) & 1) != 0) << (7 - tmp_bit_counter);
      tmp_bit_counter++;

      if (tmp_bit_counter == 8)
      {
        // if 8 bits are written add the char to the string and renew the temp
        // character
        res.push_back(tmp);
        tmp = 0;
        tmp_bit_counter = 0;
      }
    }

    return res;
  }

  /**
   * This function provides will only decode 'size' number of bytes
   */
  template <typename T>
  static std::string decode(const png::image<T> &input,
                            const std::size_t &size)
  {
    // create a return string
    std::string res;

    auto tmp_bit_counter = 0;
    char tmp = 0;

    for (auto i = 0; i < size * 8; ++i)
    {
      // set bitwise the information into a char
      tmp |= ((Channel()(input, i) & 1) != 0) << (7 - tmp_bit_counter);
      tmp_bit_counter++;

      if (tmp_bit_counter == 8)
      {
        // if 8 bits are written add the char to the string and renew the temp
        // character
        res.push_back(tmp);
        tmp = 0;
        tmp_bit_counter = 0;
      }
    }

    return res;
  }
};