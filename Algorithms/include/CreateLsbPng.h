#pragma once

#include <LSB.h>
#include <png++/png.hpp>

namespace Validation
{

template <typename Channel>
struct CreateLsbPng
{

private:
    const std::string input_name;
    png::image<png::rgb_pixel> input_image;
    png::image<png::rgb_pixel> output_image;

    const png::rgb_pixel black = png::rgb_pixel(0, 0, 0);
    const png::rgb_pixel white = png::rgb_pixel(255, 255, 255);

    void process()
    {
        output_image = png::image<png::rgb_pixel>(input_image.get_width(),
                                                  input_image.get_height());

        std::size_t counter = 0;
        for (auto row = 0; row < input_image.get_height(); ++row)
        {
            for (auto column = 0; column < input_image.get_width(); ++column)
            {
                auto &input_pic_elem = Channel()(input_image, counter);
                auto &output_pic_elem = output_image[row][column];

                output_pic_elem = (input_pic_elem & 1) ? white : black;

                counter++;
            }
        }
    }

public:
    CreateLsbPng(const std::string &i) : input_name(i)
    {
        input_image.read(input_name);
        process();
    }

    void store(const std::string &output_name)
    {

        output_image.write(output_name);
    }
};

} // end namespace Validation